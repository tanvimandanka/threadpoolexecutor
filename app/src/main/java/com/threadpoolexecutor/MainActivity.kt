package com.threadpoolexecutor

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import java.util.concurrent.*

class MainActivity : AppCompatActivity() {

    /**
     * Gets the number of available cores
     */
    val NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors()

    // Sets the amount of time an idle thread waits before terminating
    val KEEP_ALIVE_TIME = 1000

    // Sets the time unit
    val KEEP_ALIVE_TIME_UNIT = TimeUnit.MICROSECONDS

    // Used to update UI with work progress
    var count = 0

    // Thread pool executor
    lateinit var mThreadPoolExectutor : ThreadPoolExecutor

    // This is the runnable task that we will run 100 times

    var runnable = Runnable {
        kotlin.run {
            Thread.sleep(500)

            runOnUiThread({
                kotlin.run {
                    count++
                    var msg = ""
                    if (count < 100) {
                        msg = "working"
                    } else {
                        msg = "done"
                        mThreadPoolExectutor.shutdown()
                    }
                    if (Thread.interrupted() == false)
                        updateStatus(msg + count)
                }
            })
        }
    }

    private fun updateStatus(msg: String) {
        text.setText(msg)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    // perform work using single thread0
    fun buttonClickSingleThread(view: View) {
        count = 0
        var mSingleThreadExecutor = Executors.newSingleThreadExecutor();

        for (i in 0..100) {
            mSingleThreadExecutor.execute(runnable)
        }
    }

    // perfomr work using thread pool
    fun buttonClickThreadPool(view: View) {
        count = 0
         mThreadPoolExectutor = ThreadPoolExecutor(NUMBER_OF_CORES + 5,
                NUMBER_OF_CORES + 8,
                KEEP_ALIVE_TIME.toLong(), KEEP_ALIVE_TIME_UNIT, LinkedBlockingDeque<Runnable>())
        for (i in 0..100) {
            mThreadPoolExectutor.execute(runnable)
        }
    }
}
